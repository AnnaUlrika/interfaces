package Interface;

/**
 * Created by anna on 2016-06-08.
 */

public interface Vehicle {

    public String design();

    public int engine();
}
