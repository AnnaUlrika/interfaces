package Interface;

/**
 * Created by anna on 2016-06-08.
 */
// hej

public class Car implements Vehicle {

    private String color;
    private String name;
    private int id;

    //constructor
    public Car(String color, int id) {
        this.color = color;
        this.id = id;
    }

    @Override
    public String design() {
        return color;
    }


    @Override
    public int engine() {
        return id;
    }
}
