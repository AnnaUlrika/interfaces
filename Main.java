package Interface;

/**
 * Created by anna on 2016-06-08.
 */
public class Main {

    public static void main(String[] args) {


        Car Audi = new Car("red", 347);
        Vehicle Nissan = new Car("grey", 7925);

        MotorCycle Honda = new MotorCycle("blue",800, 345);

        Vehicle BMW1 = new MotorCycle("Green", 600, 3475);

        printInfo(Honda);
        printInfo(Audi);

        System.out.println(Audi.design());
        System.out.println(Nissan.engine());
        System.out.println(BMW1.engine());
    }

    public static void printInfo(Vehicle vehicle) {
        System.out.println("\nEngine id: " + vehicle.engine());
        System.out.println("Color: " + vehicle.design());
    }
}
