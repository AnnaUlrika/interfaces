package Interface;

/**
 * Created by anna on 2016-06-08.
 */
public class MotorCycle implements Vehicle {

    private String color;
    private int weight;
    private int id;

    //constructor
    public MotorCycle(String color, int weight, int id) {
        this.color = color;
        this.weight = weight;
        this.id = id;
    }


    @Override
    public String design() {
        //System.out.println("MC color: ");
        return color;
    }


    @Override
    public int engine() {
        //System.out.println("MC engine id: " + id);
        return id;

    }
}
